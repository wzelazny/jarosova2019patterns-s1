Daily weather data from [agri4cast](http://agri4cast.jrc.ec.europa.eu/DataPortal/Index.aspx?o=d) database.
Subset used in the paper by Jarošova et al. 2019 *Patterns and Predictions of Barley Yellow Dwarf Virus Vector, Cereal Aphid Migrations in Central Europe.*
